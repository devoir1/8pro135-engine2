﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LeaderBord : MonoBehaviour
{   
    [SerializeField]
    private GameObject Panel;
    [SerializeField]
    public GameObject Panel2;
    public void OpenPanel()
    {

        if (Panel.activeSelf == false)
        {
            Panel.SetActive(true);

            Panel2.GetComponent<CanvasGroup>().alpha = 0.2f;
            Panel2.GetComponent<CanvasGroup>().interactable = false;
        }
        else if(Panel.activeSelf == true)
        {
            Panel.SetActive(false);

            Panel2.GetComponent<CanvasGroup>().alpha = 1f;
            Panel2.GetComponent<CanvasGroup>().interactable = true;
        }

    }
}
