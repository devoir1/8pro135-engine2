﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public PlayerInputs playerInputsScript;
    public static AudioClip footstep, collect;
    static AudioSource audioSrc;
    private bool canPlaySound = true;
    void Start()
    {
        footstep = Resources.Load<AudioClip>("footstep");
        collect = Resources.Load<AudioClip>("collect");
        audioSrc = GetComponent<AudioSource>();

        audioSrc.Play();
    }

    public IEnumerator footsteps()
    {
        while(playerInputsScript.isRunning)
        {
            if (canPlaySound)
            {
                audioSrc.PlayOneShot(footstep);
                canPlaySound = false;
                yield return new WaitForSeconds(.35f);
                canPlaySound = true;
            }
            yield return null;
        }

    }

    public void PlaySound(string sound)
    {
        switch (sound)
        {
            case "collect":
                audioSrc.PlayOneShot(collect);
                break;

            default:
                Debug.LogError("error : wrong argument for PlaySound function");
                break;
        }
    }
}
