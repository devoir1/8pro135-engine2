﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float seconds = 0f;
    private float minutes = 0f;
    private Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        seconds += Time.deltaTime;
        if(seconds >= 60)
        {
            minutes += 1f;
            seconds = 0f;
        }
        text.text = "Timer : " + minutes.ToString() + ":" + seconds.ToString("F2");

    }
}
